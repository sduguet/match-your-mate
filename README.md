 # Hackathon Symfony

### Lancer le projet 

- make up
- make bash
- cd hackathon
- sf d:m:m
- sf d:f:l
- http://hackathon.localhost:8000

### Profils

#### Admin
- admin@admin.fr
- admin

#### Joueur
- joueur1@joueur.fr
- joueur

### Api Riot

Notre projet utilise l'api riot cependant dans le cadre du hackathon nous n'avons pas eu le temps d'avoir une clé api riot qui fonctionne constamment. Notre clé api fonctionne seulement 24 h, si vous souhaitez accéder à tout la contenue du site contacter Rémi Hay--Rimbault ou Gabriel Gaume afin qu'il vous génère une nouvelle clé api.

### Chat

Le chat n'a pas pu être fini à temps, vous pouvez le trouver en l'état sur la branch remi.
Il est quasiment fini mais il faut recharger la page pour voir les messages apparaitrent.

Merci de votre compréhension
L'équipe [k] 
