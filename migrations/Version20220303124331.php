<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220303124331 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE article (id INT AUTO_INCREMENT NOT NULL, utilisateur_id INT NOT NULL, title VARCHAR(255) NOT NULL, excerpt LONGTEXT NOT NULL, description LONGTEXT NOT NULL, img VARCHAR(255) NOT NULL, INDEX IDX_23A0E66FB88E14F (utilisateur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE session (id INT AUTO_INCREMENT NOT NULL, type_id INT NOT NULL, id_creator_id INT NOT NULL, created_at DATETIME NOT NULL, is_launched TINYINT(1) NOT NULL, INDEX IDX_D044D5D4C54C8C93 (type_id), INDEX IDX_D044D5D4C4A88E71 (id_creator_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, nb_players INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE utilisateur_session (utilisateur_id INT NOT NULL, session_id INT NOT NULL, INDEX IDX_AA4F2246FB88E14F (utilisateur_id), INDEX IDX_AA4F2246613FECDF (session_id), PRIMARY KEY(utilisateur_id, session_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E66FB88E14F FOREIGN KEY (utilisateur_id) REFERENCES utilisateur (id)');
        $this->addSql('ALTER TABLE session ADD CONSTRAINT FK_D044D5D4C54C8C93 FOREIGN KEY (type_id) REFERENCES type (id)');
        $this->addSql('ALTER TABLE session ADD CONSTRAINT FK_D044D5D4C4A88E71 FOREIGN KEY (id_creator_id) REFERENCES utilisateur (id)');
        $this->addSql('ALTER TABLE utilisateur_session ADD CONSTRAINT FK_AA4F2246FB88E14F FOREIGN KEY (utilisateur_id) REFERENCES utilisateur (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE utilisateur_session ADD CONSTRAINT FK_AA4F2246613FECDF FOREIGN KEY (session_id) REFERENCES session (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE utilisateur ADD username VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE utilisateur_session DROP FOREIGN KEY FK_AA4F2246613FECDF');
        $this->addSql('ALTER TABLE session DROP FOREIGN KEY FK_D044D5D4C54C8C93');
        $this->addSql('DROP TABLE article');
        $this->addSql('DROP TABLE session');
        $this->addSql('DROP TABLE type');
        $this->addSql('DROP TABLE utilisateur_session');
        $this->addSql('ALTER TABLE utilisateur DROP username');
    }
}
